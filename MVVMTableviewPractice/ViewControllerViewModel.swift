//
//  ViewControllerViewModel.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 18/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class ViewcontrollerViewModel {
    //MARK: - Outputs
    let tableItemTypes: [CellFunctions.Type] = [CellOneViewModel.self, CellTwoViewModel.self, CellThreeViewModel.self]
    
    private(set) var tableItems = [CellFunctions]()

    
    init() {
        self.assignTableViewCells()
    }
    
    func assignTableViewCells() {
        self.tableItems = self.setup()
    }
}

extension ViewcontrollerViewModel {
    func setup() -> [CellFunctions] {
        return (0...3).map { value in
            if (value == 0 || value == 1) {
                return CellOneViewModel(imageValue: "promotion", title: "Title \(value + 1)", description: "Description \(value + 1)")
            } else if (value == 2) {
                return CellTwoViewModel(image: UIImage(named: "promotion")!)
            } else {
                return CellThreeViewModel(dontKnowButtonAction: {
                    print("Don't know action button clicked")
                }, getQuoteButtonAction: {
                    print("Get Quote action button clicked")
                })
            }
        }
    }
    
}
