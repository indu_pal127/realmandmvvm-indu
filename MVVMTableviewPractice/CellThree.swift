//
//  CellThree.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellThree: UITableViewCell {

    @IBOutlet weak var registrationTextfield: UITextField!
    @IBOutlet weak var dontKnowButton: UIButton!
    @IBOutlet weak var getQuoteButton: UIButton!
    
    var viewModel : CellThreeViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func prepare(viewModel: CellThreeViewModel) {
//        viewModel.registrationValue = self.registrationTextfield.text ?? ""
        self.viewModel = viewModel
        self.dontKnowButton.addTarget(self, action: #selector(CellThree.dontKnowAction), for: .touchUpInside)
        self.getQuoteButton.addTarget(self, action: #selector(CellThree.getQuoteAction), for: .touchUpInside)
    }
    
    func dontKnowAction() {
        self.viewModel.dontKnowAction?()
    }
    
    func getQuoteAction() {
        if self.viewModel.validate(text: registrationTextfield.text ?? "") {
            self.viewModel.getQuoteAction?()
        } else {
            print("Failed!")
        }
    }
}
