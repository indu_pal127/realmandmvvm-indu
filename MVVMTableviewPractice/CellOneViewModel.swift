//
//  CellOneViewModel.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellOneViewModel {
    
    let leftImage : UIImage
    let titleValue : String
    let descriptionValue : String
    
    init(imageValue: String, title: String, description: String) {
        self.leftImage = UIImage(named: imageValue)!
        self.titleValue = title
        self.descriptionValue = description
    }
}

extension CellOneViewModel: CellFunctions {
    static func registerCell(tableview: UITableView) {
        tableview.register(UINib(nibName: "CellOne", bundle: nil), forCellReuseIdentifier: "CellOne")
    }
    
    func cellInstanatiate(tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "CellOne", for: indexPath) as! CellOne
        cell.prepare(viewModel: self)
        return cell
    }
}
