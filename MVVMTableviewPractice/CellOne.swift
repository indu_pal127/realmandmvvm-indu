//
//  CellOne.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellOne: UITableViewCell {

    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func prepare(viewModel: CellOneViewModel) {
        self.leftImage.image = viewModel.leftImage
        self.titleLabel.text = viewModel.titleValue
        self.descriptionLabel.text = viewModel.descriptionValue
    }

}
