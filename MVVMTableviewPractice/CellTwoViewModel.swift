//
//  CellTwoViewModel.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellTwoViewModel {
    let mainImageValue : UIImage
    
    init(image: UIImage) {
        self.mainImageValue = image
    }
}

extension CellTwoViewModel: CellFunctions {
    static func registerCell(tableview: UITableView) {
        tableview.register(UINib(nibName: "CellTwo", bundle: nil), forCellReuseIdentifier: "CellTwo")
    }
    
    func cellInstanatiate(tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "CellTwo", for: indexPath) as! CellTwo
        cell.prepare(viewModel: self)
        return cell
    }
}
