//
//  CellFunctions.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

protocol CellFunctions {
    static func registerCell(tableview: UITableView)
    func cellInstanatiate(tableview: UITableView, indexPath: IndexPath) -> UITableViewCell
}
