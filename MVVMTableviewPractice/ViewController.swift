//
//  ViewController.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    
    var viewModel: ViewcontrollerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.showsVerticalScrollIndicator = false
        self.mainTableView.separatorStyle = .none
        self.viewModel.tableItemTypes.forEach { $0.registerCell(tableview: self.mainTableView)}
        self.mainTableView.estimatedRowHeight = 100
        self.mainTableView.rowHeight = UITableViewAutomaticDimension

        self.mainTableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.tableItems[indexPath.row]
        return (cellViewModel.cellInstanatiate(tableview: tableView, indexPath: indexPath))
    }
}

