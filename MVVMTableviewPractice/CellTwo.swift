//
//  CellTwo.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellTwo: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func prepare(viewModel: CellTwoViewModel) {
        self.mainImage.image = viewModel.mainImageValue
    }
    
}
